# paper-mpi-errors-detection-using-gnn-embedding-and-vector-embedding-over-llvm-ir-reproducibility

This repository contains the material to reproduce the experiments presented in the paper *MPI Errors Detection using GNN Embedding and Vector Embedding over LLVM IR*. 
Only the method using the IR2vec strategy is reproducible. 

# Requirements 

* **MPI**, we used the [**Openmpi**](https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.4.tar.bz2) library
* [**IR2VEC**](https://github.com/IITH-Compilers/IR2Vec)
* **LLVM 12**, [binaries for linux](https://github.com/llvm/llvm-project/releases/download/llvmorg-12.0.0/clang+llvm-12.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz)

To ensure easy reproducibility of the experiments, we use Docker. To build and run the image, use the following commands:
```shell 
cd docker-config/ && docker build -f Dockerfile -t ml .
cd ../ && docker compose run --rm shell
```

Once in the docker image, the sources directory contains all the sources of this project. A Pipfile contains requirements for the python environment of this project. Run the following commands to install the required python packages and run the virtual environment to use them:
```shell
cd sources/
pipenv install
pipenv shell
```
Don't forget to export the path to IR2VEC:
```shell
export PATH=/home/me/sources/dependencies/IR2Vec/build/bin:$PATH
```

You are good to go!


# Organisation 

- The dataset folder contains MBI and MPI-CorrBench
- The dependencies folder contains IR2Vec 
- The errors-detection folder contains the scripts to compile the codes, generate the embedding and train/test the model


# Experiments 

## Generate vector files

Codes are compiled with different options into IR files. These IR files are then processed by the IR2VEC program to convert them into vector files. The vector files are used as input to our machine learning model. The following commands generate IR and vector files:
```shell
./compiling.py -bm [BENCHMARK] # Compilation
./embedding.py -bm [BENCHMARK] # Embedding
```

## Training the model

You can control different parameters for the personal case scenario you want to have, the training model is customizable and you can choose to modify the number of training and testing folds '--n_kfold', options of compilation '--option', benchmark '--benchmark', normalization methods and other parameters.

    You'll find all the customizable parameters by running:
    ```shell
    ./train.py --help
    ```

### Intra Modeling

To train and validate our model on a standalone benchmark suite (MBI or MPI-CorrBench), we used the following commands:
```shell
./train.py -bm CORR_WOB -nf 10 -norm vector -Os -CNC
./train.py -bm MBI -nf 10 -norm vector -Os -CNC
```

### Mix Modeling

To train and validate our model over the dataset Mix composed of both MBI and MPI-CorrBench, we used the following commands:
```shell
./train.py -mix MBI CORR_WOB -nf 10 -norm vector -Os -CNC
```

### Cross Modeling

To train and validate our model over distinct datasets, we used the following commands:
```shell
./train.py -bm1 MBI -bm2 CORR_WOB -nf 10 -norm vector -Os -CNC -xpl "MBI_CORR_WOB_vector_Os_min"
./train.py -bm1 CORR_WOB -bm2 MBI -nf 10 -norm vector -Os -CNC -xpl "CORR_WOB_MBI_vector_Os_min"
```

### Ablation Study

To exclude one error:
```shell
./select_and_train.py -bm MBI -exclude -nf 2 -norm vector -Os -ncomb 1
./select_and_train.py -bm CORR_WOB -exclude -nf 2 -norm vector -Os -ncomb 1
```

To exclude two errors:
```shell
./select_and_train.py -bm MBI -exclude -nf 2 -norm vector -Os -ncomb 2
./select_and_train.py -bm CORR_WOB -exclude -nf 2 -norm vector -Os -ncomb 2
```
